﻿namespace BlockScroller
{
    public class Score
    {
        public string PlayerName { get; set; }
        public long TimePlayed { get; set; }
        public int CoinsGathered { get; set; }
        public int EnemiesDefeated { get; set; }
    }
}
