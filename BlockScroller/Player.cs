﻿using System.Collections.Generic;

namespace BlockScroller
{
    public class Player
    {
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public List<Score> Scores { get; set; }
    }
}
