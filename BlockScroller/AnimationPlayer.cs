﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace BlockScroller
{
    public class AnimationPlayer
    {
        public Animation Animation { get; private set; }
        
        public int FrameIndex { get; private set; }
        
        private float _time;
        
        public Vector2 Origin => new Vector2(Animation.FrameWidth / 2.0f, Animation.FrameHeight);

        public void PlayAnimation(Animation animation)
        {
            if (Animation == animation)
            {
                return;
            }

            Animation = animation;
            FrameIndex = 0;
            _time = 0.0f;
        }
        
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position, SpriteEffects spriteEffects)
        {
            if (Animation == null)
            {
                throw new NotSupportedException("No animation is currently playing.");
            }
            
            _time += (float)gameTime.ElapsedGameTime.TotalSeconds;
            while (_time > Animation.FrameTime)
            {
                _time -= Animation.FrameTime;
                
                if (Animation.Loop)
                {
                    FrameIndex = (FrameIndex + 1) % Animation.FrameCount;
                }
                else
                {
                    FrameIndex = Math.Min(FrameIndex + 1, Animation.FrameCount - 1);
                }
            }
            
            var source = new Rectangle(FrameIndex * Animation.FrameWidth, 0, Animation.FrameWidth, Animation.FrameHeight);
            
            spriteBatch.Draw(Animation.Texture, position, source, Color.White, 0.0f, Origin, 1.0f, spriteEffects, 0.0f);
        }
    }
}
