﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace BlockScroller
{
    internal class Santa : Hero
    {
        private Animation _idleAnimation;
        private Animation _runAnimation;
        private Animation _jumpAnimation;
        private readonly AnimationPlayer _animationPlayer;

        public bool IsOnGround { get; private set; }

        private Vector2 _velocity;

        private float _movement;
        private const float MoveStickScale = 1.0f;

        private bool _isJumping;

        private const float MoveAcceleration = 13000.0f;
        private const float MaxMoveSpeed = 1750.0f;
        private const float GroundDragFactor = 0.48f;
        private const float AirDragFactor = 0.58f;

        private const float MaxJumpTime = 0.35f;
        private const float JumpLaunchVelocity = -3500.0f;
        private const float GravityAcceleration = 3400.0f;
        private const float MaxFallSpeed = 550.0f;
        private const float JumpControlPower = 0.14f;

        public Santa(Vector2 position) : base(position)
        {
            Width = 467;
            Height = 321;
            _animationPlayer = new AnimationPlayer();
            IsOnGround = true;
        }

        public override void LoadContent(ContentManager contentManager)
        {
            _idleAnimation = new Animation(contentManager.Load<Texture2D>("Sprites/Christmas/idle"), 0.07f, true, Width, Height);
            _runAnimation = new Animation(contentManager.Load<Texture2D>("Sprites/Christmas/run"), 0.07f, true, Width, Height);
            _jumpAnimation = new Animation(contentManager.Load<Texture2D>("Sprites/Christmas/jump"), 0.1f, false, Width, Height);
            _animationPlayer.PlayAnimation(_idleAnimation);
        }

        public override void Update(GameTime gameTime, ButtonPressed buttonPressed)
        {
            if (buttonPressed.Equals(ButtonPressed.Left))
            {
                _movement = -1.0f;
            }
            else if (buttonPressed.Equals(ButtonPressed.Right))
            {
                _movement = 1.0f;
            }

            _isJumping = buttonPressed.Equals(ButtonPressed.Up);

            ApplyPhysics(gameTime);

            if (IsOnGround)
            {
                _animationPlayer.PlayAnimation(Math.Abs(_velocity.X) - 0.02f > 0 ? _runAnimation : _idleAnimation);
            }

            _movement = 0.0f;
            _isJumping = false;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _animationPlayer.Draw(gameTime, spriteBatch, Position, Velocity.X < 0 ? SpriteEffects.FlipHorizontally : SpriteEffects.None);
        }

        public override void ApplyPhysics(GameTime gameTime)
        {
            var elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            var previousPosition = Position;

            _velocity.X += _movement * MoveAcceleration * elapsed;

            if (IsOnGround)
            {
                _velocity.X *= GroundDragFactor;
            }
            else
            {
                _velocity.X *= AirDragFactor;
            }

            _velocity.X = MathHelper.Clamp(_velocity.X, -MaxMoveSpeed, MaxMoveSpeed);

            Position += Velocity * elapsed;
            Position = new Vector2((float)Math.Round(Position.X), (float)Math.Round(Position.Y));
        }
    }
}
