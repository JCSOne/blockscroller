﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    public class Tile
    {
        public Rectangle Bounds { get; private set; }
        public Vector2 Position { get; set; }

        public Tile(Rectangle bounds, Vector2 position)
        {
            Bounds = bounds;
            Position = position;
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice, Texture2D texture)
        {
            spriteBatch.Draw(
                texture,
                Position,
                Bounds,
                Color.White,
                0.0f,
                Vector2.Zero,
                1f,
                SpriteEffects.None,
                0.0f);
        }
    }
}
