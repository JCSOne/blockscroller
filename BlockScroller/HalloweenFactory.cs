﻿using Microsoft.Xna.Framework;

namespace BlockScroller
{
    public class HalloweenFactory : SeasonFactory
    {
        private Jack Jack;
        private HalloweenMap HalloweenMap;

        public override Hero CreateHero(Vector2 position)
        {
            return new Jack(position);
        }

        public override Map CreateMap()
        {
            return new HalloweenMap();
        }
    }
}
