﻿using Microsoft.Xna.Framework;

namespace BlockScroller
{
    public class ChristmasFactory : SeasonFactory
    {
        private Santa Santa;
        private ChristmasMap ChristmasMap;

        public override Hero CreateHero(Vector2 position)
        {
            return new Santa(position);
        }

        public override Map CreateMap()
        {
            return new ChristmasMap();
        }
    }
}
