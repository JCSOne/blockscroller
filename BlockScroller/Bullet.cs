﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    public class Bullet
    {
        public Vector2 Position { get; set; }
        public Texture2D Texture { get; set; }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }

        public void CheckCollision()
        {

        }
    }
}
