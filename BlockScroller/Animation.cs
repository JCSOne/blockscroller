﻿using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    public class Animation
    {
        public Texture2D Texture { get; }

        public float FrameTime { get; }

        public bool Loop { get; }

        public int FrameCount => Texture.Width / FrameWidth;

        public int FrameWidth { get; }

        public int FrameHeight { get; }

        public Animation(Texture2D texture, float frameTime, bool loop, int frameWidth, int frameHeight)
        {
            Texture = texture;
            FrameTime = frameTime;
            Loop = loop;
            FrameWidth = frameWidth;
            FrameHeight = frameHeight;
        }
    }
}
