﻿using Microsoft.Xna.Framework;

namespace BlockScroller
{
    public abstract class SeasonFactory
    {
        public abstract Hero CreateHero(Vector2 position);
        public abstract Map CreateMap();
    }
}
