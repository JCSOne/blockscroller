﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    public abstract class Map
    {
        public Texture2D Texture { get; set; }
        public int TileWidth { get; set; }
        public int TileHeight { get; set; }
        public Tile[,] Tiles { get; set; }

        public abstract void Init();
        public abstract void LoadContent(ContentManager contentManager);
        public abstract void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice);
    }
}
