﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    internal class HalloweenMap : Map
    {
        public HalloweenMap() : base()
        {
            TileWidth = 128;
            TileHeight = 128;
        }

        public override void Init()
        {
            var prueba = new[,]
            {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 14, 15 },
                { 0, 0, 14, 15, 16, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 1, 2, 2, 2, 3, 0, 1, 2, 3 }
            };

            Tiles = new Tile[prueba.GetLength(0), prueba.GetLength(1)];

            for (var i = 0; i < prueba.GetLength(0); i++)
            {
                for (var j = 0; j < prueba.GetLength(1); j++)
                {
                    if (prueba[i, j] == 1)
                    {
                        var bounds = new Rectangle(0, 0, 128, 128);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                    else if (prueba[i, j] == 2)
                    {
                        var bounds = new Rectangle(128, 0, 128, 128);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                    else if (prueba[i, j] == 3)
                    {
                        var bounds = new Rectangle(256, 0, 128, 128);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                    else if (prueba[i, j] == 14)
                    {
                        var bounds = new Rectangle(128, 384, 128, 128);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                    else if (prueba[i, j] == 15)
                    {
                        var bounds = new Rectangle(256, 384, 128, 128);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                    else if (prueba[i, j] == 16)
                    {
                        var bounds = new Rectangle(384, 384, 128, 128);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                    else
                    {
                        var bounds = new Rectangle(256, 128, 0, 0);
                        Tiles[i, j] = new Tile(bounds, new Vector2(j * 128, i * 128));
                    }
                }
            }
        }

        public override void LoadContent(ContentManager contentManager)
        {
            Texture = contentManager.Load<Texture2D>("Sprites/Halloween/tiles");
        }

        public override void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            for (var i = 0; i < Tiles.GetLength(0); i++)
            {
                for (var j = 0; j < Tiles.GetLength(1); j++)
                {
                    Tiles[i, j].Draw(spriteBatch, graphicsDevice, Texture);
                }
            }
        }
    }
}
