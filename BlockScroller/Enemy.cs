﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    public class Enemy
    {
        public int Index { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        private Vector2 _position;
        public Vector2 Position => _position;
        public bool IsRunning { get; private set; }
        public bool RunningRight { get; private set; }
        public Vector2 Movement { get; set; }
        public Texture2D Texture { get; set; }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }
    }
}
