﻿namespace BlockScroller
{
    public enum ButtonPressed
    {
        None,
        Left,
        Right,
        Up
    }
}
