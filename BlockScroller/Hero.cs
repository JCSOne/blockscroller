﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BlockScroller
{
    public abstract class Hero
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }

        public Hero(Vector2 position)
        {
            Position = position;
            Velocity = Vector2.Zero;
        }

        public abstract void LoadContent(ContentManager contentManager);
        public abstract void Update(GameTime gameTime, ButtonPressed buttonPressed);
        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        public abstract void ApplyPhysics(GameTime gameTime);
    }
}
