﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System;

namespace BlockScroller
{
    public partial class VirtualGamePad
    {
        private readonly Vector2 _baseScreenSize;
        private Matrix _globalTransformation;
        private readonly Texture2D _texture;

        private float _secondsSinceLastInput;
        private float _opacity;

        public VirtualGamePad(Vector2 baseScreenSize, Matrix globalTransformation, Texture2D texture)
        {
            _baseScreenSize = baseScreenSize;
            _globalTransformation = Matrix.Invert(globalTransformation);
            _texture = texture;
            _secondsSinceLastInput = float.MaxValue;
        }

        public void NotifyPlayerIsMoving()
        {
            _secondsSinceLastInput = 0;
        }

        public void Update(GameTime gameTime)
        {
            var secondsElapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _secondsSinceLastInput += secondsElapsed;

            _opacity = _secondsSinceLastInput < 4 ? Math.Max(0, _opacity - secondsElapsed * 4) : Math.Min(1, _opacity + secondsElapsed * 2);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            var spriteCenter = new Vector2(64, 64);
            var color = Color.Multiply(Color.White, 0.5f);

            spriteBatch.Draw(_texture, new Vector2(64, _baseScreenSize.Y - 64), null, color, -MathHelper.PiOver2, spriteCenter, 1, SpriteEffects.None, 0);
            spriteBatch.Draw(_texture, new Vector2(192, _baseScreenSize.Y - 64), null, color, MathHelper.PiOver2, spriteCenter, 1, SpriteEffects.None, 0);
            spriteBatch.Draw(_texture, new Vector2(_baseScreenSize.X - 128, _baseScreenSize.Y - 128), null, color, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
        }

        public ButtonPressed GetState(TouchCollection touchState)
        {
            var pressed = ButtonPressed.None;

            foreach (var touch in touchState)
            {
                if (touch.State != TouchLocationState.Moved && touch.State != TouchLocationState.Pressed)
                {
                    continue;
                }

                var pos = touch.Position;
                Vector2.Transform(ref pos, ref _globalTransformation, out pos);

                if (pos.X < 128)
                {
                    pressed = ButtonPressed.Left;
                }
                else if (pos.X < 256)
                {
                    pressed = ButtonPressed.Right;
                }
                else if (pos.X >= _baseScreenSize.X - 128)
                {
                    pressed = ButtonPressed.Up;
                }
            }

            return pressed;
        }
    }
}
