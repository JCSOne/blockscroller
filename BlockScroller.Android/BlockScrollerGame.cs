using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace BlockScroller
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class BlockScrollerGame : Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Hero _hero;
        private Map _map;
        private VirtualGamePad _virtualGamePad;
        private readonly Vector2 _baseScreenSize = new Vector2(800, 480);
        private Matrix _globalTransformation;

        // We store our input states so that we only poll once per frame, 
        // then we use the same input state wherever needed
        private ButtonPressed _buttonPressed;

        public BlockScrollerGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            _graphics.IsFullScreen = true;
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.PreferredBackBufferHeight = 480;
            _graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;
            _graphics.ApplyChanges();

            _hero = new Hero(new Vector2(100, 657))
            {
                Width = 193,
                Height = 254,
            };

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            var horScaling = GraphicsDevice.PresentationParameters.BackBufferWidth / _baseScreenSize.X;
            var verScaling = GraphicsDevice.PresentationParameters.BackBufferHeight / _baseScreenSize.Y;
            var screenScalingFactor = new Vector3(horScaling, verScaling, 1);
            _globalTransformation = Matrix.CreateScale(screenScalingFactor);

            _hero.LoadContent(Content);
            _virtualGamePad = new VirtualGamePad(_baseScreenSize, _globalTransformation, Content.Load<Texture2D>("Sprites/VirtualControlArrow"));
            _map = new Map()
            {
                Texture = Content.Load<Texture2D>("Sprites/Level/tiles"),
                TileHeight = 128,
                TileWidth = 128
            };
            _map.Init();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            // TODO: Add your update logic here
            HandleInput(gameTime);
            _hero.Update(gameTime, _buttonPressed);

            if (_hero.Velocity != Vector2.Zero)
                _virtualGamePad.NotifyPlayerIsMoving();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            _spriteBatch.Begin();
            _hero.Draw(gameTime, _spriteBatch);
            _map.Draw(_spriteBatch, GraphicsDevice);
            _virtualGamePad.Draw(_spriteBatch);
            _spriteBatch.End();

            base.Draw(gameTime);
        }

        private void HandleInput(GameTime gameTime)
        {
            var touchState = TouchPanel.GetState();
            _buttonPressed = _virtualGamePad.GetState(touchState);

            _virtualGamePad.Update(gameTime);
        }
    }
}
