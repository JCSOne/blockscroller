﻿using System.Linq;
using Microsoft.Xna.Framework.Input.Touch;

namespace BlockScroller
{
    public static class TouchCollectionExtensions
    {
        public static bool AnyTouch(this TouchCollection touchState)
        {
            return touchState.Any(location => location.State == TouchLocationState.Pressed || location.State == TouchLocationState.Moved);
        }
    }
}