﻿using System;

namespace BlockScroller
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class BlockScroller
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            var game = new BlockScrollerGame();
            game.Run();
        }
    }
#endif
}
